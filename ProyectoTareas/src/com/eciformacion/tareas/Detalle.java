package com.eciformacion.tareas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Detalle {
	private static int contador;
	private int id;
	private String nombre;
	private int idTarea;
	private boolean realizado;
	
	public Detalle(String nombre,int idTarea,boolean realizado) {
		this.id=contador;
		contador++;
		this.nombre=nombre;
		this.idTarea=idTarea;
		this.realizado=realizado;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdTarea() {
		return idTarea;
	}

	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}

	public String isRealizado() {
		if(realizado) {
			return "si";
		}
		else
			return "no";
	}

	public void setRealizado(boolean realizado) {
		this.realizado = realizado;
	}

	public static int getContador() {
		return contador;
	}

	public static void setContador(int contador) {
		Detalle.contador = contador;
	}
	
	public int realizado() {
		if(realizado)
		return 1;
		else
			return 0;
	}
	
}//Fin detalle
