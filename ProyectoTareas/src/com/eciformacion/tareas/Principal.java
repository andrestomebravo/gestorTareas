package com.eciformacion.tareas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		GestorTarea gt;
		Scanner leer=new Scanner(System.in);
		int opcion=0,id;
		boolean realizado;
		String nombre;
		try {
			Class.forName("com.mysql.jdbc.Driver");	}
		catch(ClassNotFoundException ex){System.out.println("NO EXISTE EL DRIVER");}
		
		Connection conn;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tareas","root","");
			gt=new GestorTarea(conn);
			gt.cargarArray();
			
		while(opcion!=5) {
			System.out.println("�Que quieres hacer?\n1.-A�adir tarea\n2.-A�adir detalle de tarea\n3.-Completar detalle\n4.-Ver tareas\n5.-Salir");
			opcion=leer.nextInt();
			leer.nextLine();
			switch(opcion){
			case 1:
				System.out.println("Introduce nombre de la tarea:");
				nombre=leer.nextLine();
				gt.a�adirTarea(new Tareas(nombre));
				System.out.println("Se ha a�adido");
				break;
			case 2:
				System.out.println("Introduce el id de la tarea:");
				int idt=leer.nextInt();
				if(idt<=gt.getTareas().size()) {
					Tareas aux=gt.obtenerTarea(idt-1);
					System.out.println("�Cuantos detalles vas a a�adir?");
					int cant=leer.nextInt();
					leer.nextLine();
					for(int i=0;i<cant;i++) {
						System.out.println("Introduce nombre del detalle:");
						nombre=leer.nextLine();
						Detalle detalle=new Detalle(nombre,idt,false);
						gt.a�adirDetalles(detalle,conn,aux);
					}
				}//fin if
				break;
			case 3:
				System.out.println("Introduce el id del detalle completado:");
				int idd=leer.nextInt();
				if(idd<Detalle.getContador()) {
				for(int i=0;i<gt.getTareas().size();i++) {
					for(int j=0;j<gt.getTareas().get(i).getDetalles().size();j++) {
						if(gt.getTareas().get(i).getDetalles().get(j).getId()==idd) {
							//gt.getTareas().get(i).getDetalles().get(j).completarTarea(conn);
							gt.completarTarea(conn, gt.getTareas().get(i).getDetalles().get(j));
							break;
							}//fin if
						}//fin for
					}//fin for
				}//fin if
				break;
			case 4:
				gt.getTareas().stream().forEach(i ->{
					System.out.println("La tarea: "+i.getNombre()+" ,con id: "+i.getId());
					i.getDetalles().stream().forEach(n ->{
						System.out.println("id: "+n.getId()+"\tnombre: "+n.getNombre()+"\trealizada: "+n.isRealizado());
					});
					System.out.println();
				});
				break;
		}//fin switch
		}//fin while
		}catch (SQLException e1) {e1.printStackTrace();}
	}//fin main

}//fin principal
