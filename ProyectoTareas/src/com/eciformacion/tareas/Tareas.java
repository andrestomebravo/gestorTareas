package com.eciformacion.tareas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Tareas {

	private static int contador;
	private int id;
	private String nombre;
	private ArrayList<Detalle> detalles;
	
	public Tareas(String nombre) {
		detalles=new ArrayList();
		this.id=contador;
		this.contador++;
		this.nombre=nombre;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Detalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(ArrayList<Detalle> detalles) {
		this.detalles = detalles;
	}

	public void añadirDetallesArray(Detalle detalle) {
		detalles.add(detalle);	
	}
	
	public static int getContador() {
		return contador;
	}

	public static void setContador(int contador) {
		Tareas.contador = contador;
	}
	
}//fin Tareas
