package com.eciformacion.tareas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GestorTarea {

	private ArrayList<Tareas>  tareas;
	private Connection conn;

	public GestorTarea(Connection conn) {
		this.tareas=new ArrayList();
		this.conn=conn;
	}
	
	public void cargarArray() throws SQLException {
		Tareas auxt=new Tareas("");
		int idtarea=0;
		Detalle auxd;
		Statement st = conn.createStatement();
		ResultSet rs=st.executeQuery("SELECT * FROM tareas;");	
		Statement std=conn.createStatement();
		ResultSet rsd=std.executeQuery("SELECT * FROM detalle where idtarea="+idtarea+";");
		
		while(rs.next()) {
			auxt=new Tareas(rs.getString(2));			//creo la tarea,asi que se aumenta el contador
			idtarea=rs.getInt(1);
			rsd=std.executeQuery("SELECT * FROM detalle where idtarea="+idtarea+";");	
			while(rsd.next()) {
				auxd=new Detalle(rsd.getString(3),rsd.getInt(2),rsd.getBoolean(4));
				auxt.añadirDetallesArray(auxd);
				}
			tareas.add(auxt);
			}
		rs.close();
		rsd.close();
	}
	
	public void añadirTarea(Tareas tarea) throws SQLException {
		tareas.add(tarea);
		PreparedStatement ps=conn.prepareStatement("INSERT INTO tareas VALUES("+tarea.getId()+",'"+tarea.getNombre()+"');");
		ps.executeUpdate();			//actualizo
	}
	
	public Tareas obtenerTarea(int id) {
		return tareas.get(id);
	}

	public ArrayList<Tareas> getTareas() {
		return tareas;
	}

	public void setTareas(ArrayList<Tareas> tareas) {
		this.tareas = tareas;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
	
	public void añadirDetalles(Detalle detalle,Connection conn,Tareas aux) throws SQLException {
		aux.getDetalles().add(detalle);
		PreparedStatement ps=conn.prepareStatement("INSERT INTO detalle VALUES("+detalle.getId()+","+detalle.getIdTarea()+",'"+detalle.getNombre()+"',"+detalle.realizado()+");");
		ps.executeUpdate();			
	}
	
	public void completarTarea(Connection conn,Detalle detalle) throws SQLException {
		detalle.setRealizado(true);
		PreparedStatement ps=conn.prepareStatement("UPDATE detalle  SET realizado=true where id="+detalle.getId()+";");
		ps.executeUpdate();		
	}
}//fin gestorTareas
